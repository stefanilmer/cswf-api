const mongoose = require("mongoose");
const logger = require("./src/config/appconfig").logger;

const app = require("./src/config/app");

// since app inherits from Event Emitter, we can use this to get the app started
// after the database is connected
app.on("mongooseConnected", function() {
  const PORT = process.env.PORT || 5000;

  app.listen(PORT, () => {
    logger.info(`server is listening on port ${PORT}`);
  });
});

console.log(process.env.STATUS);

let connectionString;
if (process.env.STATUS === "production") {
  logger.info("Production: Connecting to remote database...");
  connectionString = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_NAME}-lnuj7.mongodb.net/test?retryWrites=true&w=majority`;
} else {
  logger.info("Development: Connecting to local database...");
  connectionString = "mongodb://localhost/CSWF_API";
}

// connect to the database
mongoose
  .connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => {
    logger.info("MongoDB connection established");

    // fire the event that the app is ready to listen
    app.emit("mongooseConnected");
  })
  .catch(err => {
    logger.error("MongoDB connection failed");
    logger.error(err);
  });
