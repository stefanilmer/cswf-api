const mongoose = require("mongoose");
const HeroSchema = require("./schemas/hero.schema");

const Hero = mongoose.model("item", HeroSchema);

module.exports = Hero;
