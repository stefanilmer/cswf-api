const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const GearSchema = new Schema({
  helm: {
    tpye: ItemSchema,
    enum: [null, "Helm"],
    default: null
  },
  shoudlers: {
    tpye: ItemSchema,
    enum: [null, "Shoulders"],
    default: null
  },
  chestplate: {
    tpye: ItemSchema,
    enum: [null, "Chestplate"],
    default: null
  },
  gloves: {
    tpye: ItemSchema,
    enum: [null, "Gloves"],
    default: null
  },
  legplates: {
    tpye: ItemSchema,
    enum: [null, "Legplates"],
    default: null
  },
  boots: {
    tpye: ItemSchema,
    enum: [null, "Boots"],
    default: null
  },
  mainWeapon: {
    tpye: ItemSchema,
    enum: [null, "MainWeapon"],
    default: null
  },
  secondaryWeapon: {
    tpye: ItemSchema,
    enum: [null, "SecondaryWeapon"],
    default: null
  }
});

function autoPopulate(next) {
  this.populate({
    path: "helm",
    model: "item"
  });

  this,
    populate({
      path: "shoulders",
      model: "item"
    });

  this.populate({
    path: "chestplate",
    model: "item"
  });

  this,
    populate({
      path: "gloves",
      model: "item"
    });

  this.populate({
    path: "legplates",
    model: "item"
  });

  this,
    populate({
      path: "boots",
      model: "item"
    });

  this.populate({
    path: "mainWeapon",
    model: "item"
  });

  this,
    populate({
      path: "secondaryWeapon",
      model: "item"
    });

  next();
}

GearSchema.pre("findOne", autoPopulate).pre("find", autoPopulate);

module.exports = GearSchema;
