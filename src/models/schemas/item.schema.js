const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ItemSchema = new Schema({
  Name: {
    type: String,
    required: [true, "A item name is required."]
  },

  type: {
    type: String,
    required: [true, "An item type is required."],
    enum: [
      "Helm",
      "Shoulders",
      "Chestplate",
      "Gloves",
      "Legplates",
      "Boots",
      "MainWeapon",
      "SecondaryWeapon"
    ]
  },

  itemScore: {
    type: Number,
    required: [true, "A Item score is required"]
  },

  itemLevel: {
    type: Number,
    required: [true, "A Item level is required"]
  },

  rarity: {
    type: String,
    required: [true, "A Item rarity is required"],
    enum: ["Normal", "Magic", "Rare", "Epic", "Legendary"]
  },

  goldPrice: {
    type: Number,
    required: [true, "A Item price is required"]
  }
});

module.exports = ItemSchema;
