const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    required: [true, "A username is required."],
    unique: [true, "This username is already in use."]
  },

  password: {
    type: String,
    required: [true, "A password is required"]
  },

  email: {
    type: String,
    required: [true, "A Email is required."],
    unique: [true, "This Email is already in use."]
  },

  heroes: {
    type: [Schema.Types.ObjectId],
    ref: "hero",
    default: []
  }
});

function autoPopulate(next) {
  this.populate({
    path: "heroes",
    model: "hero"
  });

  next();
}

UserSchema.pre("findOne", autoPopulate).pre("find", autoPopulate);

UserSchema.virtual("HeroCount").get(function() {
  return this.heroes.length;
});

UserSchema.plugin(uniqueValidator);
module.exports = UserSchema;
