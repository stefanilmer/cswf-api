const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const Schema = mongoose.Schema;

// Size of heroes inventory
const inventorySize = 20;

const HeroSchema = new Schema({
  name: {
    type: String,
    required: [true, "A name is required."],
    unique: [true, "This name is already in use."]
  },

  createdDate: {
    type: Date,
    default: Date.now(),
    required: [true, "A created date is required."]
  },

  gender: {
    type: String,
    required: [true, "A gender is required"]
  },

  gold: {
    type: Number,
    default: 0,
    min: 0,
    max: 2500000,
    required: [true, "gold is required."]
  },

  heroPower: {
    type: Number,
    default: 100,
    required: [true, "A hero power is required."]
  },

  heroLevel: {
    type: Number,
    defaul: 1,
    required: [true, "A hero level is required."]
  },

  heroExperience: {
    type: Number,
    default: 0,
    required: [true, "Hero experience is required"]
  },

  inventory: {
    type: [Schema.Types.ObjectId],
    ref: "item",
    default: [],
    validate: [inventoryLimit, `${path} exceeds the limit of ${inventorySize}`]
  },

  gear: GearSchema
});

function inventoryLimit(val) {
  return val.length <= inventorySize;
}

function autoPopulate(next) {
  this.populate({
    path: "inventory",
    model: "item"
  });

  this,
    populate({
      path: "gear",
      model: "item"
    });

  next();
}

HeroSchema.pre("findOne", autoPopulate).pre("find", autoPopulate);

HeroSchema.virtual("inventoryCount").get(function() {
  return this.inventory.length;
});

HeroSchema.plugin(uniqueValidator);
module.exports = HeroSchema;
