const mongoose = require("mongoose");
const ItemSchema = require("./schemas/item.schema");

const Item = mongoose.model("item", ItemSchema);

module.exports = Item;
